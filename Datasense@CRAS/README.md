Este directorio contiene los resultados de entrenamiento del modelo de red neuronal "Datasense@CRAS", por tanto, hay tres carpetas (train, train2 y train3), 
ya que, el entrenamiento se ha hecho con las versiones de Yolov8 (n-s-m), así mismo, este modelo en versión Yolov8 se ha convertido a formato openvino, 
donde la carpeta weights contiene las versiones onnx, bin y xml.

Además, se adjunta una imagen (best.onnx.svg) que contiene las capas del modelo de red neuronal, donde puede observarse de manera ilustrativa las diferentes capas durante el proceso de entrenamiento.